interface ILayerWithLegend {
  isLegendExpanded: boolean;
  wasLegendExpanded: boolean;
}

export default ILayerWithLegend;
