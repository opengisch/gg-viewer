import SearchComponent from '../search/component';

class MobileSearchComponent extends SearchComponent {
  templateUrl = './template.html';
  styleUrl = './style.css';
}

export default MobileSearchComponent;
