## Description

_(What is this Merge-Request for)_

## Job done

_(Explain shortly was was done, and what are the important things to know about those changes)_

## Definition of Done

### For the Developer:

- [ ] Code compiles and conforms to defined coding standards (eslint, tsc, prettier).
- [ ] If necessary (at the developer's discretion), unit tests have been added for critical parts.
- [ ] All unittests run without error.
- [ ] Sonar-Scan returns no new issues or security hotspots.
- [ ] Documentation has been updated where necessary (see documentation repository).
- [ ] Merge-Request contains a few explanations of what was done.
- [ ] A Reviewer has been assigned to the Merge-Request.

### For the Reviewer:

- [ ] New code was reviewed.
- [ ] If comments could be added on unclear code, the developer has been informed.
- [ ] If unitests could be added in certain places, the developer has been informed.
- [ ] Merge-Request was merged on main branch.
- [ ] All pipelines work.
- [ ] The changes were tested on demo environment.
- [ ] Application performance has not been degraded by the modifications.
